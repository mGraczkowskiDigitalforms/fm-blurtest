//
//  ViewController.swift
//  FMBlurTest
//
//  Created by Tomislav Grbin on 26/09/14.
//  Copyright (c) 2014 Tomislav Grbin. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {

    let kInterpolateStagesWithAlpha = true
    let kScrollViewTravel = 200.0
    let kMaximumBlurRadius = 25.0
    let kNumberOfStages = 10
    
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var blurredImages : [UIImage] = []
    
    var originalImage : UIImage
    
    required init(coder aDecoder: NSCoder) {
        originalImage = UIImage(named: "screen")!
        
        super.init(coder: aDecoder)
        
        initBlurredImages()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.contentSize = CGSize(width: 320.0, height: self.view.bounds.height + CGFloat(kScrollViewTravel))
        
        if kInterpolateStagesWithAlpha == false {
            secondImageView.hidden = true
        }
    }
    
    func initBlurredImages() {
        var stamp = NSDate()
        
        blurredImages.append(originalImage)
        
        for i in 1...kNumberOfStages {
            var radius = Double(i) * kMaximumBlurRadius / Double(kNumberOfStages)
            var blurredImage = blurOriginalImageWithRadius(radius)
            blurredImages.append(blurredImage)
            
            if i == kNumberOfStages {
                blurredImages.append(blurredImage)
            }
        }
        
        NSLog("generation of stages took \(-stamp.timeIntervalSinceNow) seconds")
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        var r = Double(scrollView.contentOffset.y / CGFloat(kScrollViewTravel))
        var blur = max(0, min(1, r)) * Double(kNumberOfStages)
        var blurIndex = Int(blur)
        var blurRemainder = blur - Double(blurIndex)
        
        firstImageView.image = blurredImages[blurIndex]
        
        if kInterpolateStagesWithAlpha == true {
            secondImageView.image = blurredImages[blurIndex + 1]
            secondImageView.alpha = CGFloat(blurRemainder)
        }
    }
    
    func blurOriginalImageWithRadius(radius: Double) -> UIImage {
        return originalImage.applyBlurWithRadius(CGFloat(radius), tintColor: nil, saturationDeltaFactor: 1.0, maskImage: nil)
    }
    
    // simple paging
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        var y = Double(targetContentOffset.memory.y)
        y = (y < kScrollViewTravel / 2.0) ? 0: kScrollViewTravel
        targetContentOffset.memory.y = CGFloat(y)
    }
}
