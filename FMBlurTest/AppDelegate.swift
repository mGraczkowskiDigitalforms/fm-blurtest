//
//  AppDelegate.swift
//  FMBlurTest
//
//  Created by Tomislav Grbin on 26/09/14.
//  Copyright (c) 2014 Tomislav Grbin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        application.statusBarHidden = true
        
//        var benchmark = BlurBenchmark()
//        benchmark.run()
        
        return true
    }
}
